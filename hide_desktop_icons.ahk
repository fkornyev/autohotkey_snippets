; Hide the desktop icons with double-click on Windows 7
; source: http://www.autohotkey.com/board/topic/38006-double-click-desktop-to-hide-icons/
~LButton::
 If ( A_PriorHotKey = A_ThisHotKey && A_TimeSincePriorHotkey < 400 ) {
   WinGetClass, Class, A
   If Class in Progman,WorkerW
     SetTimer, ToggleDesktopIcons, -50
 }
Return

ToggleDesktopIcons:

var := (flag=0)? "Show" : "Hide"

flag := !flag
Run, ping,, min

WinExist("ahk_class Progman") 
Control,%var%,, SysListView321

Return