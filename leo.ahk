Capslock::
SetTitleMatchMode 2
Send ^c
sleep, 200
IfWinExist leo.org
{
 WinActivate leo.org
 sleep 1000
 Send ^a
 Send ^v
 Send {Enter}
}
Else
{
 Run http://dict.leo.org/#/search=%clipboard%
}
Return

^2::
Send ^c
Run https://translate.google.com/#de/en/%clipboard%
Return

^3::
SetTitleMatchMode 2
Send ^c
winactivate, SZTAKI
Send ^v
Send {Enter}
Return